<div class="my-2 shadow text-white bg-dark p-1" id="requestContainer{{$mode}}">
  <div class="d-flex justify-content-between mb-2 d-none">
    <table class="ms-1">
      <tr>
        <td class="align-middle name">Name</td>
        <td class="align-middle"> -</td>
        <td class="align-middle email">Email</td>
        <td class="align-middle"></td>
      </tr>
    </table>
    @if ($mode == 'sent')
      <button class="btn btn-danger me-1">
        Withdraw Request
      </button>
    @else
      <button class="btn btn-primary me-1">
        Accept
      </button>
    @endif
  </div>
</div>

<div class="d-flex justify-content-center mt-2 py-3" id="load_more_request_{{$mode}}" data-page="2">
  <button class="btn btn-primary" onclick="getMoreRequests('{{$mode}}')">Load more</button>
</div>