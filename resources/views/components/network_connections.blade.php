<div class="row justify-content-center mt-5">
  <div class="col-12">
    <div class="card shadow text-white bg-dark">
      <div class="card-header">Coding Challenge - Network connections</div>
      <div class="card-body">
        <div class="btn-group w-100 mb-3" role="group" aria-label="Basic radio toggle button group">
          <input type="radio" class="btn-check" name="btnradio" id="btnradio1" autocomplete="off" checked>
          <label class="btn btn-outline-primary" for="btnradio1" id="get_suggestions_btn">
            Suggestions (<span>0</span>)
          </label>

          <input type="radio" class="btn-check" name="btnradio" id="btnradio2" autocomplete="off">
          <label class="btn btn-outline-primary" for="btnradio2" id="get_sent_requests_btn">
            Sent Requests (<span>0</span>)
          </label>

          <input type="radio" class="btn-check" name="btnradio" id="btnradio3" autocomplete="off">
          <label class="btn btn-outline-primary" for="btnradio3" id="get_received_requests_btn">
            Received Requests(<span>0</span>)
          </label>

          <input type="radio" class="btn-check" name="btnradio" id="btnradio4" autocomplete="off">
          <label class="btn btn-outline-primary" for="btnradio4" id="get_connections_btn">
            Connections (<span>0</span>)
          </label>
        </div>
        <hr>

        <div id="get_suggestions_content">
          <x-suggestion/>
        </div>

        <div id="get_sent_requests_content" style="display: none;">
          <x-request :mode="'sent'"/>
        </div>

        <div id="get_received_requests_content" style="display: none;">
          <x-request :mode="'received'"/>
        </div>

        <div id="get_connections_content" style="display: none;">
          <x-connection/>
        </div>

        <div id="skeleton">
          @for ($i = 0; $i < 3; $i++)
            <x-skeleton/>
          @endfor
        </div>

      </div>
    </div>
  </div>
</div>