<div class="my-2 shadow text-white bg-dark p-1" id="connections_container">
  <div class="d-none">
    <div class="d-flex justify-content-between mb-2">
      <table class="ms-1">
        <tr>
          <td class="align-middle name">Name</td>
          <td class="align-middle"> -</td>
          <td class="align-middle email">Email</td>
          <td class="align-middle"></td>
        </tr>
      </table>
      <section>
        <button style="width: 220px" id="get_connections_in_common_" class="btn btn-primary get_connections_in_common" type="button"
                data-bs-toggle="collapse" data-bs-target="#collapse_" aria-expanded="false" aria-controls="collapseExample">
          Connections in common (<span>0</span>)
        </button>
        <button class="btn btn-danger me-1 remove_connection_btn">Remove Connection</button>
      </section>
    </div>

    <div class="collapse" id="collapse_">
      <div class="px-5 mb-2">
        <x-connection_in_common />
      </div>
      <div id="connections_in_common_skeletons_"> @for ($i = 0; $i < 3; $i++)<x-skeleton/> @endfor </div>
      <div class="d-flex justify-content-center w-100 py-2 load_more_connections_in_common d-none">
        <button class="btn btn-sm btn-primary">Load more</button>
      </div>
    </div>
  </div>
</div>

<div class="d-flex justify-content-center mt-2 py-3" id="load_more_connections" data-page="2">
  <button class="btn btn-primary" onclick="getMoreConnections()">Load more</button>
</div>