<?php

Route::controller(\App\Http\Controllers\ConnectionController::class)
    ->group(function () {
        Route::get('/', 'index')->name('index'); // getConnections() in main.js
        Route::delete('/{connectionId}/remove', 'destroy')->name('destroy'); // removeConnection() in main.js
        Route::get('/{connectionId}/common', 'show')->name('show'); // getConnectionsInCommon() in main.js
    });

Route::controller(\App\Http\Controllers\ConnectionRequestController::class)
    ->name("requests.")
    ->prefix("requests")
    ->group(function () {
        Route::get('/', 'index')->name('index'); // getSuggestions() in main.js
        Route::get('/send/{receiverId}', 'sendRequest')->name('send'); // sendRequest() in main.js

        // function getRequests(mode) mina main.js
        Route::get('/sent', 'sentRequests')->name('sent');
        Route::get('/received', 'receivedRequests')->name('received');

        // withdrew and accept request
        Route::delete('/{receiverId}/{requestId}/withdraw', 'withdrawRequest')->name('withdraw'); // removeConnection(userId, connectionId)
        Route::post('/{senderId}/{requestId}/accept', 'acceptRequest')->name('accept'); // acceptRequest(userId, requestId) in main.js
    });