var skeletonId = 'skeleton';
var contentId = 'content';
var skipCounter = 0;
var takeAmount = 10;


function getRequests(mode) {
  $("#skeleton").fadeIn(100);

  const templateRow = $(`#requestContainer${mode}`);
  templateRow.find('div:not(.d-none)').remove();

  $.ajax({
    url: 'connections/requests/' + mode,
    type: 'GET',
    dataType: 'json',
    success: function(res) {

      res.data.forEach(function (res) {
        // Clone the template row
        var row = templateRow.find("div.d-none").clone();

        row.removeClass("d-none");

        // Set the name and email values
        row.find(".name").text(res.name);
        row.find(".email").text(res.email);
        row.find("button").on('click', (e) => {
          if (mode === "sent")
            deleteRequest(res.receiver_id, res.id);
          if (mode === "received")
            acceptRequest(res.sender_id, res.id);
          e.target.setAttribute('disabled', false);
        });

        // Append the row to the container
        $(`#requestContainer${mode}`).append(row);
      });

      const load_more_request_ = $(`#load_more_request_${mode}`);
      load_more_request_.attr('data-page', 2);
      if (parseInt(res.last_page) === 1) {
        load_more_request_.addClass("d-none");
      } else {
        load_more_request_.removeClass("d-none");
      }
      $(`#get_${mode}_requests_btn span`).text(res.total);
    },
    error: function(xhr, status, error) {
      // Handle the error here
      console.error(error);
    }
  }).then(() => {
    $("#skeleton").fadeOut(100);
  });
}

function getMoreRequests(mode) {
  $("#skeleton").fadeIn(100);
  const load_more_btn = $(`#load_more_request_${mode}`);
  const templateRow = $(`#requestContainer${mode}`);

  // Make an AJAX request to retrieve more suggestions
  $.ajax({
    url: `/connections/requests/${mode}?page=${load_more_btn.attr('data-page')}`,
    type: "GET",
    dataType: "json",
    success: function (res) {

      res.data.forEach(function (item) {
        // Clone the template row
        var row = templateRow.find("div.d-none").clone();

        // Remove the suggestions class from the cloned row
        row.removeClass("d-none");

        // Set the name and email values
        row.find(".name").text(item.name);
        row.find(".email").text(item.email);
        row.find("button").on('click', (e) => {
          if (mode === "sent")
            deleteRequest(item.receiver_id, item.id);
          if (mode === "received")
            acceptRequest(item.sender_id, item.id);
          e.target.setAttribute('disabled', false);
        });

        // Append the row to the suggestions container
        $(`#requestContainer${mode}`).append(row);
      });

      load_more_btn.attr('data-page', parseInt(load_more_btn.attr('data-page')) + 1);
      // Optional: Update the "Load more" button visibility based on the response
      if (parseInt(res.last_page) !== 1 && res.to < res.total) {
        load_more_btn.removeClass("d-none");
      } else {
        load_more_btn.addClass("d-none");
      }
      $(`#get_${mode}_requests_btn span`).text(res.total);
    },
    error: function (xhr, status, error) {
      // Handle any errors that occur during the AJAX request
      console.error(error);
    }
  }).then(() => {
    $("#skeleton").fadeOut(100);
  });
}

function getConnections() {
  $("#skeleton").fadeIn(100);
  const load_more_connections = $("#load_more_connections");

  $.ajax({
    url: "/connections",
    type: "GET",
    dataType: "json",
    success: function (res) {
      const templateRow = $("#connections_container");
      templateRow.find("div:not(.d-none,.d-none > *,.collapse,.collapse > *)").remove();

      res.data.forEach(function (conn) {
        // Clone the template row
        var row = templateRow.find("div.d-none").first().clone();

        // Remove the suggestions class from the cloned row
        row.removeClass("d-none");

        // Set the name and email values
        row.find("button").attr('data-bs-target', `#collapse_${conn.id}`);
        row.find("div.collapse").attr('id', `collapse_${conn.id}`);

        row.find(".name").text(conn.name);
        row.find(".email").text(conn.email);

        row.find("button.remove_connection_btn").on('click', (e) => {
          removeConnection(null, conn.id)
          e.target.setAttribute('disabled', false);
        });
        row.find("button.get_connections_in_common").on('click', (e) => {
          getConnectionsInCommon(null,conn.id);
        });

        // Append the row to the suggestions container
        $("#connections_container").append(row);
      });

      load_more_connections.attr('data-page', 2);
      if (parseInt(res.last_page) === 1) {
        load_more_connections.addClass("d-none");
      } else {
        load_more_connections.removeClass("d-none");
      }
      $("#get_connections_btn span").text(res.total);
    },
    error: function (xhr, status, error) {
      // Handle any errors that occur during the AJAX request
      console.error(error);
    }
  }).then(() => {
    $("#skeleton").fadeOut(100);
  });
}

function getMoreConnections() {
  $("#skeleton").fadeIn(100);
  const load_more_btn = $("#load_more_connections");

  $.ajax({
    url: `/connections?page=${load_more_btn.attr('data-page')}`,
    type: "GET",
    dataType: "json",
    success: function (res) {
      const templateRow = $("#connections_container");

      res.data.forEach(function (conn) {
        // Clone the template row
        var row = templateRow.find("div.d-none").first().clone();

        // Remove the suggestions class from the cloned row
        row.removeClass("d-none");

        // Set the name and email values
        row.find(".name").text(conn.name);
        row.find(".email").text(conn.email);
        row.find("button.remove_connection_btn").on('click', (e) => {
          removeConnection(null, conn.id)
          e.target.setAttribute('disabled', false);
        });
        row.find("button.get_connections_in_common").on('click', (e) => {
          getConnectionsInCommon(null,conn.id);
        });

        // Append the row to the suggestions container
        $("#connections_container").append(row);
      });

      load_more_btn.attr('data-page', parseInt(load_more_btn.attr('data-page')) + 1);
      // Optional: Update the "Load more" button visibility based on the response
      if (parseInt(res.last_page) !== 1 && res.to < res.total) {
        load_more_btn.removeClass("d-none");
      } else {
        load_more_btn.addClass("d-none");
      }
      $("#get_suggestions_btn span").text(res.total);
    },
    error: function (xhr, status, error) {
      // Handle any errors that occur during the AJAX request
      console.error(error);
    }
  }).then(() => {
    $("#skeleton").fadeOut(100);
  });
}

function getConnectionsInCommon(userId, connectionId) {
  const templateRow = $(`#collapse_${connectionId}`);

  $.ajax({
    url: `/connections/${connectionId}/common`,
    type: 'GET',
    success: function(res) {
      templateRow.find('div').first().find("div:not(.d-none)").remove();

      res.data.forEach(function (conn) {
        // Clone the template row
        var row = templateRow.find("div.d-none").first().clone();

        // Remove the suggestions class from the cloned row
        row.removeClass("d-none");

        // Set the name and email values
        row.text(`${conn.name} - ${conn.email}`);

        // Append the row to the suggestions container
        templateRow.find('div').first().prepend(row);
      });

      // command
      const load_more_connection_common = templateRow.find("div.load_more_connections_in_common");
      load_more_connection_common.attr('data-page', 2);
      if (parseInt(res.last_page) === 1) {
        load_more_connection_common.addClass("d-none");
      } else {
        load_more_connection_common.removeClass("d-none");
      }
      $(`button[data-bs-target="#collapse_${connectionId}"].get_connections_in_common span`).text(res.total);
    },
    error: function() {
      // An error occurred.
    }
  });

  templateRow.find(".load_more_connections_in_common").first().on('click', (e) => {
    getMoreConnectionsInCommon(userId, connectionId);
  });
}

function getMoreConnectionsInCommon(userId, connectionId) {
  const templateRow = $(`#collapse_${connectionId}`);
  const load_more_connection_common = templateRow.find("div.load_more_connections_in_common").first();

  $.ajax({
    url: `/connections/${connectionId}/common?page=${load_more_connection_common.attr('data-page')}`,
    type: 'GET',
    success: function(res) {

      res.data.forEach(function (conn) {
        // Clone the template row
        var row = templateRow.find("div.d-none").first().clone();

        // Remove the suggestions class from the cloned row
        row.removeClass("d-none");

        // Set the name and email values
        row.text(`${conn.name} - ${conn.email}`);

        // Append the row to the suggestions container
        templateRow.find("div").first().append(row);
      });

      // command
      load_more_connection_common.attr('data-page', parseInt(load_more_connection_common.attr('data-page')) + 1);
      // Optional: Update the "Load more" button visibility based on the response
      if (parseInt(res.last_page) !== 1 && res.to < res.total) {
        load_more_connection_common.removeClass("d-none");
      } else {
        load_more_connection_common.addClass("d-none");
      }
      $(`button[data-bs-target="#collapse_${connectionId}"].get_connections_in_common span`).text(res.total);
    },
    error: function() {
      // An error occurred.
    }
  });
}

function getSuggestions() {
  $("#skeleton").fadeIn(100);
  const templateRow = $("#suggestionsContainer");
  templateRow.find('div:not(.d-none)').remove();

  $.ajax({
    url: "/connections/requests",
    type: "GET",
    dataType: "json",
    success: function (res) {

      res.data.forEach(function (suggest) {
        // Clone the template row
        var row = templateRow.find("div.d-none").clone();

        // Remove the suggestions class from the cloned row
        row.removeClass("d-none");

        // Set the name and email values
        row.find(".name").text(suggest.name);
        row.find(".email").text(suggest.email);
        row.find("button.send-request").on('click', (e) => {
          sendRequest(null, suggest.id);
          e.target.setAttribute('disabled', false);
        });

        // Append the row to the suggestions container
        $("#suggestionsContainer").append(row);
      });

      const load_more_suggestion = $(`#load_more_suggestion`);
      load_more_suggestion.attr('data-page', 2);
      if (parseInt(res.last_page) === 1) {
        load_more_suggestion.addClass("d-none");
      } else {
        load_more_suggestion.removeClass("d-none");
      }
      $("#get_suggestions_btn span").text(res.total);
    },
    error: function (xhr, status, error) {
      // Handle any errors that occur during the AJAX request
      console.error(error);
    }
  }).then(() => {
    $("#skeleton").fadeOut(100);
  });
}

function getMoreSuggestions() {
  $("#skeleton").fadeIn(100);

  const load_more_btn = $("#load_more_suggestion");
  const templateRow = $("#suggestionsContainer div.d-none");

  // Make an AJAX request to retrieve more suggestions
  $.ajax({
    url: `/connections/requests?page=${load_more_btn.attr('data-page')}`,
    type: "GET",
    dataType: "json",
    success: function (res) {

      res.data.forEach(function (item) {
        // Clone the template row
        var row = templateRow.clone();

        // Remove the suggestions class from the cloned row
        row.removeClass("d-none");

        // Set the name and email values
        row.find(".name").text(item.name);
        row.find(".email").text(item.email);
        row.find("button.send-request").on('click', (e) => {
          sendRequest(null, item.id);
          e.target.setAttribute('disabled', false);
        });

        // Append the row to the suggestions container
        $("#suggestionsContainer").append(row);
      });

      load_more_btn.attr('data-page', parseInt(load_more_btn.attr('data-page')) + 1);
      // Optional: Update the "Load more" button visibility based on the response
      if (parseInt(res.last_page) !== 1 && res.to < res.total) {
        load_more_btn.removeClass("d-none");
      } else {
        load_more_btn.addClass("d-none");
      }
      $("#get_suggestions_btn span").text(res.total);
    },
    error: function (xhr, status, error) {
      // Handle any errors that occur during the AJAX request
      console.error(error);
    }
  }).then(() => {
    $("#skeleton").fadeOut(100);
  });
}

function sendRequest(userId, suggestionId) {
  // Make an AJAX call using jQuery
  $.ajax({
    url: `connections/requests/send/${suggestionId}`, // Replace `userId` with the actual receiver ID
    type: 'GET',
    //data: {"_token": CSRF},
    success: function(response) {
      // Handle the success response from the server
    },
    error: function(xhr, status, error) {
      // Handle any errors that occur during the request
      console.error(error);
    }
  });
}

function deleteRequest(userId, requestId) {
  $.ajax({
    url: `/connections/requests/${userId}/${requestId}/withdraw`,
    type: 'DELETE',
    data: {"_token": CSRF},
    success: function() {
      // The withdrawal request has been deleted.
    },
    error: function() {
      // An error occurred.
    }
  });
}

function acceptRequest(userId, requestId) {
  $.ajax({
    url: `/connections/requests/${userId}/${requestId}/accept`,
    type: 'POST',
    data: {"_token": CSRF},
    success: function() {
      // The request has been accepted.
    },
    error: function() {
      // An error occurred.
    }
  });
}

function removeConnection(userId, connectionId) {
  $.ajax({
    url: `/connections/${connectionId}/remove`,
    type: 'DELETE',
    data: {"_token": CSRF},
    success: function() {
      // The withdrawal request has been deleted.
    },
    error: function() {
      // An error occurred.
    }
  });
}

$(function () {
  $(document).ready(function () {
    $('label.btn').click(function () {
      var id = $(this).attr('id');

      if (id === "get_sent_requests_btn")
        getRequests("sent")
      else if (id === "get_received_requests_btn")
        getRequests("received")
      else if (id === 'get_suggestions_btn')
        getSuggestions();
      else if (id === 'get_connections_btn')
        getConnections();

      var contentId = id.replace('_btn', '_content');

      // Hide all content divs
      $('div[id$="_content"]').hide();

      // Show the corresponding content div
      $('#' + contentId).show();
    });
  });

  getSuggestions();
  getRequests("sent");
  getRequests("received");
  getConnections();
});