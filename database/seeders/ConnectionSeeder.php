<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;

class ConnectionSeeder extends Seeder
{
    public function run()
    {
        User::join('connection_requests', function ($join) {
                $join->on('users.id', '=', 'connection_requests.sender_id')
                    ->orWhere('users.id', '=', 'connection_requests.receiver_id');
            })
            ->select('users.*')
            ->distinct()
            ->chunk(100, function ($users) {
                foreach ($users as $user) {
                    for ($i=0; $i < rand(10, 30); $i++) {
                        $randomUser = $users->random();
                        \App\Models\Connection::factory()
                            ->create([
                                'user1_id' => $user->id,
                                'user2_id' => $randomUser->id,
                            ]);
                    }
                }
            });

//        User::leftJoin('connection_requests', function ($join) {
//            $join->on('users.id', '=', 'connection_requests.sender_id')
//                ->orOn('users.id', '=', 'connection_requests.receiver_id');
//        })
//            ->whereNull('connection_requests.sender_id')
//            ->whereNull('connection_requests.receiver_id')
//            ->select('users.id')
//            ->chunk(100, function ($users) {
//                foreach ($users as $user) {
//                    for ($i=0; $i < rand(1, 30); $i++) {
//                        $randomUser = $users->random();
//                        \App\Models\Connection::factory()
//                            ->create([
//                                'user1_id' => $user->id,
//                                'user2_id' => $randomUser->id,
//                            ]);
//                    }
//                }
//            });

    }
}
