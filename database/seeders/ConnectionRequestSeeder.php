<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;

class ConnectionRequestSeeder extends Seeder
{
    public function run()
    {

        $users = User::select(['id'])->inRandomOrder()->limit(30)->get();
        foreach ($users as $user) {
            foreach (User::select(['id'])->where('id', '!=', $user->id)->limit(rand(11, 20))->inRandomOrder()->pluck('id') as $u) {
                \App\Models\ConnectionRequest::factory()
                    ->create([
                        'sender_id' => $user->id,
                        'receiver_id' => $u
                    ]);
            }
        }
    }
}
