<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class ConnectionRequestFactory extends Factory
{
    public function definition()
    {
        return [
            'status' => 'pending',
            //'email' => $this->faker->unique()->safeEmail(),
        ];
    }
}
