<?php

namespace App\Http\Controllers;

use App\Models\Connection;
use Illuminate\Support\Facades\Auth;

class ConnectionController extends Controller
{
    // list all auth user connections
    function index()
    {
        // Get the authenticated user's ID
        $authId = Auth::id();

        // Retrieve connections where the authenticated user is either user1 or user2
        // Join the 'users' table to retrieve additional information about the users
        // Select the connection ID, user name, and user email
        // Paginate the results with 10 entries per page
        return Connection::where('user1_id', $authId)
            ->orWhere('user2_id', $authId)
            ->join('users', function ($join) {
                $join->on('connections.user1_id', '=', 'users.id')
                    ->orWhere('connections.user2_id', '=', 'users.id');
            })
            ->select(['connections.id', 'users.name', 'users.email'])
            ->paginate(10);
    }

    // remove connection
    public function destroy(int $connectionId)
    {
        // Get the authenticated user's ID
        $authId = Auth::id();

        // Delete the connection where the ID matches the provided connectionId
        return Connection::where(['id' => $connectionId])->delete();
    }

    // for common connections b/w users
    public function show(int $connectionId)
    {
        // Get the authenticated user's ID
        $authId = Auth::id();

        // Retrieve the user ID of the other user in the connection
        // where the connection ID matches the provided connectionId
        // and where the authenticated user is user1
        $userIds = Connection::where(['id' => $connectionId, 'user1_id' => $authId])->pluck('user2_id')->toArray();

        // Retrieve the user ID of the other user in the connection
        // where the connection ID matches the provided connectionId
        // and where the authenticated user is user2
        $userIds = array_merge($userIds, Connection::where(['id' => $connectionId, 'user2_id' => $authId])->pluck('user1_id')->toArray());

        // Retrieve connections where both user1_id and user2_id are in the $userIds array
        // Join the 'users' table to retrieve additional information about the users
        // Select the connection ID, user name, and user email
        // Paginate the results with 10 entries per page
        $connections = Connection::whereIn('user1_id', $userIds)
            ->whereIn('user2_id', $userIds)
            ->join('users', function ($join) {
                $join->on('connections.user1_id', '=', 'users.id')
                    ->orWhere('connections.user2_id', '=', 'users.id');
            })
            ->select(['connections.id', 'users.name', 'users.email'])
            ->paginate(10);

        return $connections;
    }
}