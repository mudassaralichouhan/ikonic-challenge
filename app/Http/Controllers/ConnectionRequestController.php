<?php

namespace App\Http\Controllers;

use App\Models\Connection;
use App\Models\ConnectionRequest;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;

class ConnectionRequestController extends Controller
{
    public function index()
    {
        $user = Auth::user();

        // Query to retrieve connections where the user is either user1 or user2
        $connectionQuery = Connection::where(function ($query) use ($user) {
            $query->where('connections.user1_id', $user->id)
                ->orWhere('connections.user2_id', $user->id);
        });

        // Query to retrieve invitations sent or received by the user
        $invitationQuery = $user->sentInvitations()->select('receiver_id')
            ->union($user->receivedInvitations()->select('sender_id'));

        // Query to retrieve suggested users
        $suggestedUsers = User::select(['users.id', 'users.name', 'users.email'])
            ->whereNotIn('users.id', $connectionQuery->pluck('user2_id')->toArray())
            ->whereNotIn('users.id', $invitationQuery->pluck('receiver_id')->toArray())
            ->where('users.id', '!=', $user->id)
            ->paginate(10);

        return Response::json($suggestedUsers);
    }


    public function sendRequest(int $receiverId)
    {
        // Get the authenticated user's ID
        $authId = Auth::id();

        // Check if the connection request already exists
        $existingRequest = ConnectionRequest::where('sender_id', $authId)
            ->where('receiver_id', $receiverId)
            ->first();

        if ($existingRequest)
            return $existingRequest;

        // Create a new connection request
        return ConnectionRequest::create([
            'sender_id' => $authId,
            'receiver_id' => $receiverId,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
    }

    public function sentRequests()
    {
        $authId = Auth::id();

        // Query the connection_requests table for rows where the sender_id matches the authenticated user's ID
        // and join the users table on the receiver_id column
        $connectionRequests = ConnectionRequest::where('sender_id', $authId)
            ->join('users', 'connection_requests.receiver_id', '=', 'users.id')
            ->select([
                'users.email',
                'users.name',
                'connection_requests.receiver_id',
                'connection_requests.id'
            ])
            ->where(['connection_requests.status' => 'pending'])
            ->paginate(10);

        return $connectionRequests;
    }


    public function receivedRequests()
    {
        $authId = Auth::id();

        // Retrieve connection requests where the authenticated user is the receiver
        $connectionRequests = ConnectionRequest::where('receiver_id', $authId)

            // Join the 'users' table on the 'sender_id' column of 'connection_requests' table with the 'id' column of 'users' table
            ->join('users', 'connection_requests.sender_id', '=', 'users.id')

            // Select specific columns from the joined tables
            ->select([
                'users.email',
                'users.name',
                'connection_requests.sender_id',
                'connection_requests.id'
            ])

            // Filter the requests to only include those with a 'status' of 'pending'
            ->where(['connection_requests.status' => 'pending'])

            // Paginate the results, showing 10 entries per page
            ->paginate(10);

        return $connectionRequests;
    }


    public function withdrawRequest(int $receiverId, int $requestId)
    {
        $authId = Auth::id(); // Get the authenticated user's ID

        return ConnectionRequest::where([
            'receiver_id' => $receiverId, // Check if the receiver_id matches the provided $receiverId
            'id' => $requestId, // Check if the id matches the provided $requestId
            'sender_id' => $authId, // Check if the sender_id matches the authenticated user's ID
        ])->delete(); // Delete the matching connection request and return the result
    }


    public function acceptRequest(int $senderId, $requestId)
    {
        $authId = Auth::id();

        // Create a new connection between the authenticated user and the sender
        $conn = Connection::create([
            'user1_id' => $authId,
            'user2_id' => $senderId,
        ]);

        // If the connection was successfully created, delete the corresponding connection request
        if ($conn) {
            ConnectionRequest::where([
                'sender_id' => $senderId,
                'id' => $requestId,
                'receiver_id' => $authId,
            ])->delete();
        }

        // Return the connection as a JSON response
        return Response::json($conn);
    }

}
